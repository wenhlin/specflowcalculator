
## Specflow and uia to automate win 10 calculator

First of all, thank you for taking the time to read the codes, which could be better a lot, reusing methods, creating page object models, etc.

Secondly, as my computer system is in Chinese, I could not change it into English because all my works are on it, so the parameters of the project,
such as strings, are in Chinese instead of English, so maybe you could not run the project.

But I have recorded a video in the demo.zip, which is a demo of my project.

Finally, thanks again for giving me the opportunity!
Kind regards.
Wenhui Lin.