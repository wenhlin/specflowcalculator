﻿Feature: Calculator
![Calculator](https://specflow.org/wp-content/uploads/2020/09/calculator.png)
Simple calculator for adding **two** numbers

Link to a feature: [Calculator](SpecFlowProject1/Features/Calculator.feature)
***Further read***: **[Learn more about how to generate Living Documentation](https://docs.specflow.org/projects/specflow-livingdoc/en/latest/LivingDocGenerator/Generating-Documentation.html)**

    @mytag
Scenario: Automate programmer mode
Given Launch calc
	And I navigate to programmer mode
	When I enter a value
	Then I verify HEX value is correct
	And I verify DEC value is correct
	And I verify OCT value is correct
	And I verify BIN value is correct
	And close calc


	@mytag
Scenario: Automate scientific mode
Given Launch calc
	And I navigate to scientific mode
	When perform some calculations
	Then verify the results
	And close calc


	@mytag
Scenario: Automate date calculation mode
Given Launch calc
	And I navigate to date calculation mode
	When enter dates
	Then verify results
	And close calc


	@mytag
Scenario: Automate standard mode
Given Launch calc
	And I navigate to standard mode
	When perform some calculations for standard
	Then verify the results for standard
	And cancel
	And retrieve
	And validate from history
	And close calc