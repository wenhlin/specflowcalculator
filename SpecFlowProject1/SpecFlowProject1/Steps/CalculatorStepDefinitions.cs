﻿using TechTalk.SpecFlow;
using System;
using UIDeskAutomationLib;
using System.Threading;
using NUnit.Framework;


namespace DesktopUIautomationCsharp.Steps
{
    [Binding]
    public sealed class CalculatorStepDefinitions
    {

        public static void InitializeTest()
        {
            //start process
            Engine engine = new Engine();
            int procId = engine.StartProcess("calc.exe");
            //Clear all content
            UIDA_Window window = engine.GetTopLevel("计算器");

            UIDA_Button nav = window.Button("打开导航", true);
            nav.Press();
            Thread.Sleep(1000);

            UIDA_ListItem programmerCalculator = engine.GetTopLevel("计算器").WindowAt("计算器", 2).Custom("").Window("").Pane("").Group("").ListItem("标准 计算器");
            programmerCalculator.Click();
            Thread.Sleep(1000);
        }

   
        public static void ClearContent(Engine engine)
        {
            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Window("弹出窗口").Pane("记忆").Button("清除所有记忆").Click();
            Thread.Sleep(1000);
            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Click();
            Thread.Sleep(1000);

        }
        public static void FinalizeTest()
        {
            Engine engine = new Engine();
            var windows = engine.GetTopLevelWindows("计算器");
            foreach (UIDA_Window window in windows)
            {
                window.Close();
            }
        }

        [Given(@"Launch calc")]
        public void GivenLaunchCalc()
        {
            FinalizeTest();
            Thread.Sleep(1000);
            InitializeTest();
            Thread.Sleep(100);
        }

        [Then(@"close calc")]
        public void ThenCloseCalc()
        {
            Thread.Sleep(1000);
            FinalizeTest();
        }

        [Given(@"I navigate to programmer mode")]
        public void GivenINavigateToProgrammerMode()
        {
            Engine engine = new Engine();
            UIDA_Window window = engine.GetTopLevel("计算器");
            UIDA_Button nav = window.Button("打开导航", true);
            nav.Press();
            Thread.Sleep(1000);

            UIDA_ListItem programmerCalculator = window.WindowAt("计算器", 2).Custom("").Window("").Pane("").Group("").ListItem("程序员 计算器");
            programmerCalculator.Click();
            Thread.Sleep(1000);

        }

        [When(@"I enter a value")]
        public void WhenIEnterAValue()
        {
            Engine engine = new Engine();
            UIDA_Window window = engine.GetTopLevel("计算器");
            //7
            UIDA_Button btn1 = window.Button("七", true);
            btn1.Press();
            Thread.Sleep(1000);
            //7
            UIDA_Button btn2 = window.Button("七", true);
            btn2.Press();
            Thread.Sleep(1000);

        }

        [Then(@"I verify HEX value is correct")]
        public void ThenIVerifyHEXValueIsCorrect()
        {
            Engine engine = new Engine();
            UIDA_Window window = engine.GetTopLevel("计算器");

            UIDA_RadioButton HexaDecimal = engine.GetTopLevel("计算器").WindowAt("计算器", 2)
                                            .Group("").Group("基数选择").RadioButton("十六进制 4 D ");
            HexaDecimal.Click();
            Thread.Sleep(100);

            UIDA_Button MemoryStore = window.Button("记忆存储", true);
            MemoryStore.Press();
            Thread.Sleep(1000);
            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Group("").Group("记忆控件").Button("打开记忆浮出控件").Click();
            UIDA_Label value = engine.GetTopLevel("计算器").WindowAt("计算器", 2).Window("弹出窗口").Pane("记忆").List("").ListItem("").Label("4D");
            string actualValue = value.GetText();
            Thread.Sleep(1000);

            string expectedValue = "4D";
            Assert.AreEqual(actualValue, expectedValue);
            Thread.Sleep(1000);
            ClearContent(engine);

        }

        [Then(@"I verify DEC value is correct")]
        public void ThenIVerifyDECValueIsCorrect()
        {
            Engine engine = new Engine();
            UIDA_Window window = engine.GetTopLevel("计算器");
            Thread.Sleep(1000);
            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Group("").Group("基数选择").RadioButton("十进制 77").Click();
            Thread.Sleep(1000);

            UIDA_Button MemoryStore = window.Button("记忆存储", true);
            MemoryStore.Press();

            Thread.Sleep(1000);
            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Group("").Group("记忆控件").Button("打开记忆浮出控件").Click();
            Thread.Sleep(1000);
            UIDA_Label value = engine.GetTopLevel("计算器").WindowAt("计算器", 2).Window("弹出窗口").Pane("记忆").List("").ListItem("").Label("77");
            string actualValue = value.GetText();
            Thread.Sleep(1000);

            string expectedValue = "77";
            Assert.AreEqual(actualValue, expectedValue);
            Thread.Sleep(1000);
            ClearContent(engine);
        }

        [Then(@"I verify OCT value is correct")]
        public void ThenIVerifyOCTValueIsCorrect()
        {
            Engine engine = new Engine();
            UIDA_Window window = engine.GetTopLevel("计算器");


            UIDA_RadioButton octaCal = engine.GetTopLevel("计算器").WindowAt("计算器", 2)
                                            .Group("").Group("基数选择").RadioButton("八进制 1 1 5 "); ;
            octaCal.Click();
            Thread.Sleep(1000);

            UIDA_Button MemoryStore = window.Button("记忆存储", true);
            MemoryStore.Press();

            Thread.Sleep(1000);
            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Group("").Group("记忆控件").Button("打开记忆浮出控件").Click();
            UIDA_Label value = engine.GetTopLevel("计算器").WindowAt("计算器", 2).Window("弹出窗口").Pane("记忆").List("").ListItem("").Label("115");
            string actualValue = value.GetText();
            Thread.Sleep(1000);

            string expectedValue = "115";
            Assert.AreEqual(actualValue, expectedValue);
            Thread.Sleep(1000);
            ClearContent(engine);
        }

        [Then(@"I verify BIN value is correct")]
        public void ThenIVerifyBINValueIsCorrect()
        {
            Engine engine = new Engine();
            UIDA_Window window = engine.GetTopLevel("计算器");


            UIDA_RadioButton octaCal = engine.GetTopLevel("计算器").WindowAt("计算器", 2)
                                            .Group("").Group("基数选择").RadioButton("二进制 0 1 0 0  1 1 0 1 "); ;
            octaCal.Click();
            Thread.Sleep(1000);

            UIDA_Button MemoryStore = window.Button("记忆存储", true);
            MemoryStore.Press();

            Thread.Sleep(1000);
            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Group("").Group("记忆控件").Button("打开记忆浮出控件").Click();
            UIDA_Label value = engine.GetTopLevel("计算器").WindowAt("计算器", 2).Window("弹出窗口").Pane("记忆").List("").ListItem("").Label("100 1101");
            string actualValue = value.GetText();
            Thread.Sleep(1000);

            string expectedValue = "100 1101";
            Assert.AreEqual(actualValue, expectedValue);
            Thread.Sleep(1000);
            ClearContent(engine);
        }

        [Given(@"I navigate to scientific mode")]
        public void GivenINavigateToScientificMode()
        {
            Engine engine = new Engine();
            UIDA_Window window = engine.GetTopLevel("计算器");
            UIDA_Button nav = window.Button("打开导航", true);
            nav.Press();
            Thread.Sleep(1000);

            UIDA_ListItem scienticCal = window.WindowAt("计算器", 2).Custom("").Window("").Pane("").Group("").ListItem("科学 计算器");
            scienticCal.Click();
            Thread.Sleep(1000);
        }

        [When(@"perform some calculations")]
        public void WhenPerformSomeCalculations()
        {
            Engine engine = new Engine();
            UIDA_Window window = engine.GetTopLevel("计算器");

            //1st Step: Square root of X
            UIDA_Button buttonTwo = window.Button("二", true);
            buttonTwo.Press();
            Thread.Sleep(2000);
            UIDA_Button buttonFive = window.Button("五", true);
            buttonFive.Press();
            Thread.Sleep(2000);
            UIDA_Button SquareRoot = window.Button("平方根", true);
            SquareRoot.Press();
            Thread.Sleep(2000);
            UIDA_Button MemoryStore = window.Button("记忆存储", true);
            MemoryStore.Press();
            Thread.Sleep(2000);

            // 2nd Step: 10 exponent X
            buttonTwo.Press();
            Thread.Sleep(2000);
            UIDA_Button TenToExponent = window.Button("十的指数", true);
            TenToExponent.Press();
            Thread.Sleep(2000);
            UIDA_Button MemoryStore2 = window.Button("记忆存储", true);
            MemoryStore2.Press();
            Thread.Sleep(2000);

            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Group("").Group("记忆控件").Button("打开记忆浮出控件").Click();
        }

        [Then(@"verify the results")]
        public void ThenVerifyTheResults()
        {
            Engine engine = new Engine();
            //Verify 1st Step: Square root of X
            UIDA_Label MemoryLabel = engine.GetTopLevel("计算器").WindowAt("计算器", 2).Window("弹出窗口").Pane("记忆").List("").ListItemAt("", 2).Label("5");
            string actualValue = MemoryLabel.GetText();
            Thread.Sleep(2000);
            string expectedValue = "5";
            Assert.AreEqual(actualValue, expectedValue);
            Thread.Sleep(2000);

            //Verify 2nd Step: 10 exponent X
            UIDA_Label MemoryLabel2 = engine.GetTopLevel("计算器").WindowAt("计算器", 2).Window("弹出窗口").Pane("记忆").List("").ListItem("").Label("100");
            string actualValue2 = MemoryLabel2.GetText();
            Thread.Sleep(2000);
            string expectedValue2 = "100";
            Assert.AreEqual(actualValue2, expectedValue2);

            ClearContent(engine);

        }

        [Given(@"I navigate to date calculation mode")]
        public void GivenINavigateToDateCalculationMode()
        {
            Engine engine = new Engine();
            UIDA_Window window = engine.GetTopLevel("计算器");
            UIDA_Button nav = window.Button("打开导航", true);
            nav.Press();
            Thread.Sleep(1000);

            UIDA_ListItem dateCal = window.WindowAt("计算器", 2).Custom("").Window("").Pane("").Group("").ListItem("日期计算 计算器");
            dateCal.Click();
            Thread.Sleep(2000);
        }

        [When(@"enter dates")]
        public void WhenEnterDates()
        {
            Engine engine = new Engine();
            UIDA_Window window = engine.GetTopLevel("计算器");

            //Select calculation mode
            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Group("").ComboBox("计算模式").Click();

            Thread.Sleep(1000);
            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Group("").ComboBox("计算模式").ListItem("添加或减去天数").Label("添加或减去天数").Click();
            Thread.Sleep(1000);
            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Group("").RadioButton("减去").Click();
            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Group("").ComboBox("天").Click();
            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Group("").ComboBox("天").ListItem("12").Label("12").Click();
            Thread.Sleep(2000);
         
        }

        [Then(@"verify results")]
        public void ThenVerifyDayResults()
        {
            Engine engine = new Engine();
            UIDA_Label label = engine.GetTopLevel("计算器").WindowAt("计算器", 2).Group("").Label("所得日期 ‎2021‎年‎10‎月‎24‎日");
            string actualValue = label.GetText();
            Thread.Sleep(2000);
            string expectedValue = "所得日期 ‎2021‎年‎10‎月‎24‎日";
            Assert.AreEqual(actualValue, expectedValue);
            Thread.Sleep(2000);
        }


        [Given(@"I navigate to standard mode")]
        public void GivenINavigateToStandardMode()
        {
            //Once we launch calculator， default mode is standar
            Thread.Sleep(4000);
        }

        [When(@"perform some calculations for standard")]
        public void WhenPerformSomeCalculationsForStandard()
        {
            Engine engine = new Engine();
            UIDA_Window window = engine.GetTopLevel("计算器");
            Thread.Sleep(1000);
            // X
            UIDA_Button buttonTwo = window.Button("二", true);
            buttonTwo.Press();
            Thread.Sleep(1000);
            UIDA_Button Square = window.Button("平方", true);
            Square.Press();
            Thread.Sleep(1000);
            UIDA_Button MemoryStore = window.Button("记忆存储", true);
            MemoryStore.Press();
            UIDA_Button Plus = window.Button("加", true);
            Plus.Press();
            UIDA_Button buttonFive = window.Button("五", true);
            buttonFive.Press();
            Thread.Sleep(1000);
            UIDA_Button equal = window.Button("等于", true);
            equal.Press();
            Thread.Sleep(1000);
            MemoryStore.Press();
          
            Thread.Sleep(1000);
            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Group("").Group("记忆控件").Button("打开记忆浮出控件").Click();
        }

        [Then(@"verify the results for standard")]
        public void ThenVerifyTheResultsForStandard()
        {
            Engine engine = new Engine();
            UIDA_Label MemoryLabel = engine.GetTopLevel("计算器").WindowAt("计算器", 2).Window("弹出窗口").Pane("记忆").List("").ListItemAt("", 2).Label("4");
            string actualValue = MemoryLabel.GetText();
            Thread.Sleep(2000);
            string expectedValue = "4";
            Assert.AreEqual(actualValue, expectedValue);
            Thread.Sleep(2000);


            UIDA_Label MemoryLabel2 = engine.GetTopLevel("计算器").WindowAt("计算器", 2).Window("弹出窗口").Pane("记忆").List("").ListItem("").Label("9");
            string actualValue2 = MemoryLabel2.GetText();
            Thread.Sleep(2000);
            string expectedValue2 = "9";
            Assert.AreEqual(actualValue2, expectedValue2);

            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Click();
        }


        [Then(@"cancel")]
        public void ThenCancel()
        {
            Engine engine = new Engine();
            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Group("").Group("显示控件").Button("清除").Click();
        }

        [Then(@"retrieve")]
        public void ThenRetrieve()
        {
            Engine engine = new Engine();
            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Group("").Group("记忆控件").Button("记忆读出").Click();
        }

        [Then(@"validate from history")]
        public void ThenValidateFromHistory()
        {
            Engine engine = new Engine();
            //FROM HISTORY
            engine.GetTopLevel("计算器").WindowAt("计算器", 2).Group("").Button("打开历史记录浮出控件").Press();
            Thread.Sleep(1000);
            UIDA_Label MemoryLabel2 = engine.GetTopLevel("计算器").WindowAt("计算器", 2).Window("弹出窗口").Pane("历史记录").List("").ListItem("").Label("9");
            string actualValue2 = MemoryLabel2.GetText();
            Thread.Sleep(2000);
            string expectedValue2 = "9";
            Assert.AreEqual(actualValue2, expectedValue2);
        }



    }
}